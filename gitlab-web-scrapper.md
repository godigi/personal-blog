```javascript

function getAvailability(row, index, featureIndex) {
    return (features[featureIndex].getElementsByClassName('badge-container')[0].getElementsByClassName(row)[0].getElementsByClassName('badge')[index].classList.value === "badge available") ? true : false;
}

function getDescription(featureIndex){
    let descArr = features[featureIndex].getElementsByTagName('p'); 
    let desc = ''
    for(let i = 0; i < descArr.length; i++){
        desc = desc + descArr[i].textContent.replaceAll('\n','');
    }
    return desc;
}

devopsCycles = document.getElementsByClassName("list-unstyled")[0].getElementsByClassName("js-in-page-nav-item");
parentMap = {}
for (let i = 0; i < devopsCycles.length; i++){
    map = {}
    stage = devopsCycles[i].textContent.replaceAll('\n', '').toLowerCase(); 
    parentMap[stage] = map;
    d = document.getElementById(stage);
    map.title = d.getElementsByClassName('u-text-brand')[0].textContent.replaceAll('\n', '');
    map.description = d.getElementsByClassName('subhead')[0].textContent.replaceAll('\n', ''); 
    map.features = [];
    features = d.getElementsByClassName('col-xs-12 feature-contents');
    for (let i = 0; i < features.length; i++){ 
        map.features.push({
            "heading": features[i].getElementsByTagName('h4')[0].textContent.replaceAll('\n',''),
            "description": getDescription(i),
            "saas_free_availability": getAvailability('top-row', 0, i),
            "saas_preminum_availability": getAvailability('top-row', 1, i),
            "saas_ultimate_availability": getAvailability('top-row', 2, i),
            "self_managed_free_availability": getAvailability('bottom-row', 0, i),
            "self_managed_preminum_availability": getAvailability('bottom-row', 1, i),
            "self_managed_ultimate_availability": getAvailability('bottom-row', 2, i),
            "image_url": "NA",
            "documentation_link": "NA"
        });
    }
}
```
