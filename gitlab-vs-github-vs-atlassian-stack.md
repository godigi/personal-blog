### TOC 

- [Description](#description)
- [Administration and Authentication](#administration-and-authentication)
- [Team Planning and Agile Framework](#team-planning-and-agile-framework)
- [Source Code Management](#source-code-management)
- [Continuous Integration and Continuous Delivery (CI/CD)](#continuous-integration-and-continuous-delivery-cicd)
- [Performance Testing](#performance-testing)
- [Analytics](#analytics)
- [Package and container registry](#package-and-container-registry)
- [Security](#security)
- [Monitoring](#monitoring)
- [Support](#support)
- [Pricing](#pricing)
- [Takeaway](#takeaway)
- [References:](#references)

### Description 

This issue deals with comparison for paid versions for Gitlab/Github/Atlassian Stack

**Things to keep in mind while reviewing these products:**

- Administration and Authentication
- Team Planning and Agile Framework
- Source Code Management 
- Continuous Integration and Continuous Delivery 
- Performance Testing 
- Analytics
- Package and container registry 
- Security 
- Monitoring 
- Enablement 
- Support 
- Pricing 

### Administration and Authentication

**Gitlab** 
- Extensive administration and authentication feature including users, projects, rbac, 2f authentication, LDAP etc. 
- Score: 5/5

**Github** 
- Comparable to gitlab. 
- Score: 5/5

**Atlassian (Bitbucket/Jira)** 
- Comparable to gitlab. 
- Score: 5/5

**Conclusion** 

- All are comparable.

### Team Planning and Agile Framework

**Gitlab** 
- Free version has features which is sufficing our needs for single project and group boards. Premium has additional feature likes Roadmaps, Iterations, Epics etc. which can enhance our processes. Ultimate has features like requirement management and quality management which can be skipped for now. 
- Going for premium plans for these features only seems little extra to pay for. Jira from atlassian stack can be considered.
- Documentation via issues and gitlab pages seems little unorganised. Roadmaps can help us out here too. 
- Design and Product teams onboarding is little difficult. 
- Score: 3/5

**Github** 
- Out of box solution is in beta
- Features like drag and drop, expansion of issues, filtering etc seem to be missing [Link](https://github.com/orgs/github/projects/4247)
- Would need to use third party integrations for agile reporting. 
- Score: 1/5 

**Atlassian(Jira+Confluence)**
- More complete package for Agile
- Customised templates can be made
- Readymade dashboards available for management
- Easier adoption for product and design teams
- Premium pricing for $14.5 per user per month is on higher side for just agile framework
- Confluence has extra cost of $5-$10 per user per month
- Score: 4/5

**Conclusion** 

- Jira is more complete package for Agile but the overhead pricing is brings to the table needs to be taken account of. 
- Going with Gitlab Premium will provide most of the features which Jira has to offer with added advantage of single place to manage everything. But rapid feature addition happens in gitlab and also premium features are made available in free version time to time. 
- Features of JIRA missing in Gitlab: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/50478#note_95584769
- Different teams can work on Jira for a few days to see if it is easier for them or not. 

### Source Code Management 

**Gitlab** 
- All SCM features available. Push, pull, branch, merge, code reviews name any. 
- Score: 5/5

**Github** 
- All SCM features available.
- Score: 5/5

**Atlassian (Bitbucket)** 
- Most of the basic SCM features available.
- Score: 4/5

**Conclusion** 

All are comparable. 

### Continuous Integration and Continuous Delivery (CI/CD)

**Gitlab** 
- Most extensive solution providing CI/CD use cases.
- Self hosted option allows for having our in house runners. 
- Cost efficient. 
- Score: 5/5

**Github** 
- Github Actions is the recent product delivering CI/CD out of box. 
- Relatively new
- Third party integrations have their own costing. 
- Score: 3/5

**Conclusion** 
Gitlab is way ahead. 

**Atlassian (Bitbucket)** 
- Very basic CI/CD provided out of box. 
- Using Bamboo for self hosted is required. 
- Score: 2/5

### Performance Testing 

**Gitlab** 
- Provides web and load testing features out of the box. 
- Score: 4/5 

**Gitlab** 
- Third party integrations might be available. Would need to explore. 
- Score: 1/5

**Atlassian**
- Third party integrations might be available. Would need to explore. 
- Score: 1/5

**Conclusion** 
Gitlab is way ahead.

### Analytics

**Gitlab** 

- Basic Analytics is available in free edition. Agile, CI/CD, SCM etc basic dashboards available. 
- Premium has additional analytics available which can be used for managerial purposes. 
- Ultimate is too costly just to pay for graphs it offers. 
- Flexible if we self host and have our own queries. 
- Score: 3/5

**Github** 

- Basic analytics available. 
- Support for agile not there so third party libraries need to be explored. 
- Score: 1/5 

**Atlassian(Jira)** 
- Most useful in Agile
- Has almost 30 readymade graphs
- Custom dashboard availability need to be explored
- Score: 4/5

**Conclusion** 

Seems logical to start exploring Jira for analytics they have in place. We can query the gitlab db to make comparable dashboards. 

### Package and container registry 

**Gitlab**
- Out of box solution available
- Score: 5/5

**Github** 
- Provides solution
- Score: 5/5

**Atlassian**
- Missing and no updates 
- Score: 0/5

**Conclusion** 
Gitlab and github is comparable. 
### Security 

**Gitlab**
- Exhaustive list. 
- Most of them in premium and ultimate tiers. 
- Score: 4/5

**Github** 
- Has most of standard security features. 
- In teams and enterprise. 
- Score: 4/5

**Atlassian**
- Has standard features. 
- In enterprise. 
- Score: 4/5

**Conclusion**

All are comparable in once we have upgraded plan. 


### Monitoring 

**Gitlab**
- Provides error tracking (sentry), incident management and also prometheus integration. 
- Limited in scope. 
- Newrelic solves this problem for us. 
- Score: 2/5

**Github** 
- Third party integrations might be available. 
- Score: 1/5

**Atlassian**
- Third party integrations might be available. 
- Score: 1/5

### Support 

**Gitlab**

- Since it is open sources at core level, it has a large community to help out on features. 
- Premium and ultimate have good support structure. 
- Score: 4/5

**Github** 
- Managed by MS. 
- Teams and Enterprise have supports.
- Score: 4/5 

**Atlassian**
- Standard and enterprise have supports. 
- Score: 4/5

### Pricing 

**Gitlab**
- Premium at $19/m/u and ultimate at $99$/m/u/
- Premium brings multiple features. 


**Github** 
- Teams at $4/m/u and Enterprise at $21/m/u.
- Teams is comparable with gitlab free. 

**Atlassian**
- Bitbucket/Jira/Confluence have their own pricing. 
  - Bitbucket: $3/m/u for standard and $6/m/u for premium 
  - Jira: $7.5/m/u for standard and $14.5/m/u for premium 
  - Confluence: $5.5/m/u for standard and $10.5/m/u for premium 

**Conclusion** 
- Gitlab Premium at $19/m/u seems costly to start with. Ultimate is exceedingly overpriced for our setup. 
- Features Gitlab premium offers which we might use need to be evaluated comprehensively before we move to this plan.
- Teams plan for github bring little to the table in comparison to gitlab we are currently using. 
- For atlassian stack, bitbucket/jira/confluence have their own costs with limited features in case of standard edition. Also using confluence for documentation is overpriced. Jira seems to be only tool which can be used in our stack, and that too the premium version as it brings to table roadmaps and epics. 


### Takeaways

- Keep using Gitlab Core and explore all the core features. 
- Review the Gitlab premium features and see if they can be used in our stack. 
- Open sourced and Self hosting solution for gitlab makes it way more flexible. 
- For CI/CD, security, Performance testing, package registry Gitlab seems to be way more promising. It is a complete devsecops solution.
- Jira has better Agile Framework in place with features and reporting. Can be considered to be used at basic level to start with in a single team to further comprehend the usability. Personally, I liked the UI/UX, features, reporting and targeted approach Jira has to offer. Could be easier to give training to non-dev teams too. Having said that, Gitlab premium self hosted solution seems to be viable alternative to it. But it lacks somewhat in analytics in reporting. 
- Gitlab: Pricing consideration makes it difficult to choose gitlab premium right now knowing the limited features we use in it. Just going premium doesn't for Agile make sense. 
- Github: Can be skipped altogether. 
- Atlassian: Jira can integrated in our systems. But premium plan seems to be costly and standard plan doesn't bring much to the table. 



### References: 
- Features Comparison Sheet: https://docs.google.com/spreadsheets/d/116sbvVj-vKw6ZHIWBHUMJXHJxcORTax7dQ6Z0ygDivQ/edit#gid=313889062
- Devops Cycle: https://blogs.vmware.com/management/2020/03/vi-admin-to-devops.html
- Agile Development: https://www.atlassian.com/agile
- JIRA vs Gitlab: https://about.gitlab.com/devops-tools/jira-vs-gitlab/
- JIRA Alternatives: https://clickup.com/blog/jira-alternatives/
- Gitlab vs Github CI/CD Comparison: https://about.gitlab.com/devops-tools/github-vs-gitlab/ci-missing-github-capabilities
- Bitbucket vs Gitlab: https://about.gitlab.com/devops-tools/bitbucket-vs-gitlab/
- Features of JIRA missing in Gitlab: https://gitlab.com/gitlab-org/gitlab-foss/-/issues/50478#note_95584769
